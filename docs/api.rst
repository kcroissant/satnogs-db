API
===

SatNOGS DB API is a REST API that provides detailed information about Satellites and Transmitters.
This document explains how to use the API to retrieve data for your application.


Using API Data
--------------

API access is open to anyone.
All API data are freely distributed under the `CC BY-SA <https://creativecommons.org/licenses/by-sa/4.0/>`_ license.


API Reference
-------------

`Our live schema docs <https://db.satnogs.org/api/schema/docs/>`_ contain a full interactive reference of the API.
