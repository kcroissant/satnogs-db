# Generated by Django 4.0.10 on 2023-02-23 09:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0059_tle_url'),
    ]

    operations = [
        migrations.AlterField(
            model_name='satelliteentry',
            name='citation',
            field=models.CharField(default='', help_text='A reference (preferrably URL) for this entry or edit', max_length=512),
        ),
    ]
